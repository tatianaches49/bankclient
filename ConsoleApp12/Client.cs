﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class Client
    {
      
      public  event MyDelegate Notify; // объявление события
        public void Take(int sum) // метод для генерирования события
        {
            Console.WriteLine($"Сумма снятия = {sum}");
           
            if (Bank.Sum - sum > 10)
            {
                Bank.Sum -= sum;
                Console.WriteLine($"После снятие баланс = {Bank.Sum}");
            }
            else {
                Notify($"Недостаточно денег на счете. Текущий баланс: { Bank.Sum}"); ;
            }
        }

       
    }
}
