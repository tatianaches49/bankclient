﻿using System;

delegate void MyDelegate(string text);
namespace ConsoleApp12
{
    class Program
    {
      
        static void Main(string[] args)
        {
            // создание объекта
            Bank account = new Bank(100);
            // создание объекта с событием:
            Client client = new Client();
            account.Put(70);   // ложим на счет 70
            
            // добавление обработчика для события:
            client.Notify += account.show;
            // генерирование события:
            client.Take(180);

         
        }
    }
}
