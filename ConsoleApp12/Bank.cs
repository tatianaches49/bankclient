﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class Bank
    {
        // сумма на счете
        public static int Sum { get;  set; }
        // в конструкторе устанавливаем начальную сумму на счете
        public Bank(int sum) { 
            Sum = sum;
            Console.WriteLine($"На счету: {Sum}");
        }

        public void Put(int sum) // положить денежку
        {
            Sum += sum;
           Console.WriteLine($"На счет поступило: {sum}");
           Console.WriteLine($"Общий счет: {Sum}");

        }

        public void show(string msg) { // вывод сообщения при наступлении события
            Console.WriteLine(msg);
        }
        
       
           
    }
}
